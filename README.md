# Jogo Gourmet


Este é um simples jogo criado para ser avaliado pela equipe técnica da Objective Solutions. 

# Gerando versão UberJar


Navegue até a pasta do prjeto e execute na linha de comando:

```
./gradlew shaddowJar
```

ou caso windows:

```
gradlew.bat shaddowJar
```

Será gerado um arquivo UberJar/ShadowJar em **build/libs/JogoGourmet-1.0.1-jar.jar**, este é seu executável
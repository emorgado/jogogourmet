package br.com.emorgado.jogo.gourmet;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.emorgado.jogo.gourmet.model.ElementoJogo;
import br.com.emorgado.jogo.gourmet.service.JogoGourmet;

public class JogoGourmetTest {

    ElementoJogo< String >              conhecimentoInicial;
    JogoGourmet               jogo;
    JogoGourmetClienteTestImpl cliente;

    
    @Before
    public void setUp() throws Exception {

        cliente = new JogoGourmetClienteTestImpl();

        jogo = new JogoGourmet( "Lazanha", "massa", "Bolo de chocolate" );
    }

    @After
    public void tearDown() throws Exception {}

    @Test
    public void testaClienteDeTeste() {

        cliente.ensinaNome( "Joao" )
               .ensinaCaracteristica( "Alto" )
               .ensinaNome( "Julio" )
               .ensinaCaracteristica( "Baixo" );

        assertEquals( "Joao", cliente.perguntaNome() );
        assertEquals( "Alto", cliente.perguntaCaracteristica( "", "" ) );

        assertEquals( "Julio", cliente.perguntaNome() );
        assertEquals( "Baixo", cliente.perguntaCaracteristica( "", "" ) );
    }

    @Test
    public void testAcertaDePrimeira() {

        cliente.respondeSimCaracteristica()
               .respondeSimNome();

        jogo.inicia( cliente );

        assertEquals( "Lazanha", jogo.getUltimoElemento()
                                     .getNome() );

    }

    @Test
    public void testAcertaNaSegunda() {

        cliente.respondeNaoCaracteristica()
               .respondeSimNome();

        jogo.inicia(cliente);

        assertEquals( "Bolo de chocolate", jogo.getUltimoElemento().getNome() );

    }

    @Test
    public void testNaoAcertaAprendeUmNovo() {

        cliente.respondeNaoCaracteristica()
               .respondeNaoNome()
               .ensinaNome( "Salada de Fruta" )
               .ensinaCaracteristica( "Composto por frutas" );

        jogo.inicia(cliente);

        assertEquals( "Composto por frutas", jogo.getUltimoElemento().getNome() );
        assertEquals( "Salada de Fruta", jogo.getUltimoElemento().getProximo().getNome() );
        assertEquals( "Bolo de chocolate", jogo.getUltimoElemento()
                                               .getCaracteristica()
                                               .getNome() );

    }

    @Test
    public void testNaoAcertaAprendeUmNovoRecomecaAcertandoNovo() {

        cliente.respondeNaoCaracteristica()
               .respondeNaoNome()
               .ensinaNome( "Salada de Fruta" )
               .ensinaCaracteristica( "Composto por frutas" );

        jogo.inicia(cliente);

        assertEquals( "Composto por frutas", jogo.getUltimoElemento()
                                                 .getNome() );
        assertEquals( "Salada de Fruta", jogo.getUltimoElemento()
                                             .getProximo()
                                             .getNome() );
        assertEquals( "Bolo de chocolate", jogo.getUltimoElemento()
                                               .getCaracteristica()
                                               .getNome() );


        cliente.limpa()
               .respondeNaoCaracteristica()               
               .respondeSimCaracteristica()               
               .respondeSimNome();
        
        jogo.inicia(cliente);
        
        assertEquals("Salada de Fruta", jogo.getUltimoElemento().getNome());
    }

}

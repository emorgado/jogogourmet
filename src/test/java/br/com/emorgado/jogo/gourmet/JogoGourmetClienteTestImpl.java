package br.com.emorgado.jogo.gourmet;

import java.util.LinkedList;

import br.com.emorgado.jogo.gourmet.service.JogoGourmetCliente;

public class JogoGourmetClienteTestImpl
                                       implements
                                       JogoGourmetCliente {

    private LinkedList< Boolean > respostasCaracteristica = new LinkedList<>();
    private LinkedList< Boolean > respostasNome           = new LinkedList<>();
    private LinkedList< String >  nomes                   = new LinkedList<>();
    private LinkedList< String >  caracteristicas         = new LinkedList<>();

    @Override
    public boolean confirmeCaracteristica( String caracteristica ) {

        System.out.println( "O prato que você pensou é " + caracteristica + "?" );
        return respostasCaracteristica.pollLast();
    }

    @Override
    public boolean confirmaNome( String nome ) {

        System.out.println( "O prato escolhido foi " + nome + "?" );
        return respostasNome.pollLast();
    }

    @Override
    public void mostraMensagemEncontrado(String nome) {

        System.out.println( "Acertei" );
    }

    @Override
    public String perguntaNome() {

        System.out.println( "Qual prato você pensou?" );
        return this.nomes.pollLast();
    }

    @Override
    public String perguntaCaracteristica( String nomeNovo, String nomeAnterior ) {

        System.out.println( nomeNovo + " ______ mas " + nomeAnterior + " não." );
        return this.caracteristicas.pollLast();
    }

    public JogoGourmetClienteTestImpl limpa() {

        this.respostasCaracteristica.clear();
        this.respostasNome.clear();
        this.nomes.clear();
        this.caracteristicas.clear();
        return this;
    }

    public JogoGourmetClienteTestImpl respondeSimCaracteristica() {

        this.respostasCaracteristica.push( true );
        return this;
    }

    public JogoGourmetClienteTestImpl respondeNaoCaracteristica() {

        this.respostasCaracteristica.push( false );
        return this;
    }

    public JogoGourmetClienteTestImpl respondeSimNome() {

        this.respostasNome.push( true );
        return this;
    }

    public JogoGourmetClienteTestImpl respondeNaoNome() {

        this.respostasNome.push( false );
        return this;
    }

    public JogoGourmetClienteTestImpl ensinaNome( String nome ) {

        this.nomes.push( nome );
        return this;
    }

    public JogoGourmetClienteTestImpl ensinaCaracteristica( String caracteristica ) {

        this.caracteristicas.push( caracteristica );
        return this;
    }

}

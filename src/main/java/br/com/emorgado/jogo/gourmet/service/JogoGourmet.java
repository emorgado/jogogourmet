package br.com.emorgado.jogo.gourmet.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.com.emorgado.jogo.gourmet.model.ElementoJogo;
import br.com.emorgado.jogo.gourmet.model.FinalizadorComAcerto;
import br.com.emorgado.jogo.gourmet.model.FinalizadorComAprendizado;
import br.com.emorgado.jogo.gourmet.model.FinalizadorJogada;
import br.com.emorgado.jogo.gourmet.model.Opcao;

/**
 * 
 * @author emorgado
 *
 * Procurei manter a mesma estrutura do jogo anterior, para não quebrar muita coisa.
 * A ideia ainda é manter um unico objeto cliente, que possa fazer perguntas diferentes.
 * Pois usando perguntas diferentes, c
 */
public class JogoGourmet {

    private static final Logger logger = LogManager.getLogger();

    private ElementoJogo< String > conhecimentoInicial;
    private ElementoJogo< String > ultimoElemento;

    public JogoGourmet( String nomePrato, String caracteristicaPrato, String nomeProximo ) {

        logger.info( "Criando Jogo prato: {}, com caracteristica: {}, e o proximo é: {}", nomePrato, caracteristicaPrato, nomeProximo );
        this.conhecimentoInicial = new ElementoJogo< String >( caracteristicaPrato);
        this.conhecimentoInicial.setCaracteristica( new ElementoJogo< String >(  nomeProximo  ) );
        this.conhecimentoInicial.setProximo( new ElementoJogo< String >( nomePrato ) );

    }

    public void inicia( JogoGourmetCliente cliente ) {

        logger.info( "Iniciando o jogo..." );

        logger.debug( "Conhecimento inicial: {}", conhecimentoInicial );

        ElementoJogo< String > pratoAtual = conhecimentoInicial;

        while ( !pratoAtual.ehUltimo() ) {

            Opcao<ElementoJogo<String>> opcaoElemento = new Opcao<>( pratoAtual.getProximo(), pratoAtual.getCaracteristica() );

            Boolean resposta = cliente.confirmeCaracteristica( pratoAtual.getNome() );

            pratoAtual = opcaoElemento.escolhe( resposta );
        }

        Boolean resposta = cliente.confirmaNome( pratoAtual.getNome()
                                                           .toString() );

        FinalizadorComAcerto< String > acerto = new FinalizadorComAcerto<>( cliente, pratoAtual );
        FinalizadorComAprendizado< String > aprendizado = new FinalizadorComAprendizado<>( cliente, pratoAtual );

        Opcao<FinalizadorJogada< String >> opcaoFinalizacao = new Opcao<>( acerto, aprendizado);
        this.ultimoElemento = opcaoFinalizacao.escolhe( resposta ).finalizaJogada();
        
    }

    public ElementoJogo<String> getUltimoElemento() {
        return this.ultimoElemento;
    }
}

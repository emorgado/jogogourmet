package br.com.emorgado.jogo.gourmet.model;

import br.com.emorgado.jogo.gourmet.service.JogoGourmetCliente;

public class FinalizadorComAcerto<E> implements FinalizadorJogada<E> {

    private JogoGourmetCliente cliente;
    private ElementoJogo< E > elementoJogo;
    
    public FinalizadorComAcerto( JogoGourmetCliente cliente, ElementoJogo< E > elementoJogo ) {
        this.cliente = cliente;
        this.elementoJogo = elementoJogo;
    }

    @Override
    public ElementoJogo<E> finalizaJogada() {

        this.cliente.mostraMensagemEncontrado( this.elementoJogo.getNome().toString() );
        return this.elementoJogo; 
    }
    
    
    
}

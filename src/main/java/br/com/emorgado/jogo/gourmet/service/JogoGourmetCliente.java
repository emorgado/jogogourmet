package br.com.emorgado.jogo.gourmet.service;


public interface JogoGourmetCliente {

    public boolean confirmeCaracteristica( String caracteristica );

    public boolean confirmaNome( String nome );
    
    public void mostraMensagemEncontrado( String nome );

    public String perguntaNome();

    public String perguntaCaracteristica(String nomeNovo, String nomeAnterior);

}

package br.com.emorgado.jogo.gourmet.model;

public interface FinalizadorJogada<E> {

    public ElementoJogo<E> finalizaJogada();
}

package br.com.emorgado.jogo.gourmet.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ElementoJogo< E > {

    private static final Logger logger = LogManager.getLogger();
    
    public E                 nome;
    public ElementoJogo< E > caracteristica;
    public ElementoJogo< E > proximo;

    public ElementoJogo( E nome ) {
        logger.info( "Criando elemento jogo {}", nome );
        this.nome = nome;
    }

    public Boolean ehUltimo() {

        return this.proximo == null && this.caracteristica == null;
    }
    
    // Acessores

    public E getNome() {

        return nome;
    }

    public void setNome( E nome ) {

        this.nome = nome;
    }

    public ElementoJogo< E > getCaracteristica() {

        return caracteristica;
    }

    public void setCaracteristica( ElementoJogo< E > caracteristica ) {

        this.caracteristica = caracteristica;
    }

    public ElementoJogo< E > getProximo() {

        return proximo;
    }

    public void setProximo( ElementoJogo< E > proximo ) {

        this.proximo = proximo;
    }

}

package br.com.emorgado.jogo.gourmet.model;

import java.util.HashMap;
import java.util.Map;

public class Opcao< E > {

    private Map< Boolean, E > map = new HashMap<>( 2 );

    public Opcao(  E seSim, E seNao ) {

        this.map.put( true, seSim );
        this.map.put( false, seNao );
    }

    public E escolhe( Boolean resposta ) {

        return map.get( resposta );
    }

}

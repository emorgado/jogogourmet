package br.com.emorgado.jogo.gourmet.view;

import java.util.Optional;

import br.com.emorgado.jogo.gourmet.service.JogoGourmet;
import br.com.emorgado.jogo.gourmet.service.JogoGourmetCliente;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;

public class JogoGourmetViewClient
                                   extends
                                   GridPane
                                   implements
                                   JogoGourmetCliente {

    private JogoGourmet jogo;

    public JogoGourmetViewClient( JogoGourmet jogo ) {
        this.jogo = jogo;
        this.configuraTela();
    }

    private void configuraTela() {

        // ---- Grid setup....
        setHgap( 10 );
        setVgap( 10 );
        setPadding( new Insets( 25 ) );
        getColumnConstraints().add( 0, new ColumnConstraints( 200 ) );
        getColumnConstraints().add( 1, new ColumnConstraints( 50 ) );
        getColumnConstraints().add( 2, new ColumnConstraints( 200 ) );
        // setGridLinesVisible(true); // just for debug

        int rowNum = 1;

        Label lblSection = new Label( "Pense em um prato que gosta" );
        add( lblSection, 0, rowNum, 3, 1 );

        Button btnInput = new Button( "Ok" );
        btnInput.setFont( new Font( 12 ) );
        btnInput.setMaxWidth( Double.MAX_VALUE );
        btnInput.setOnAction( actionEvent -> {
            this.jogo.inicia( this );
        } );
        add( btnInput, 1, ++rowNum );
    }

    @Override
    public boolean confirmeCaracteristica( String caracteristica ) {

        Alert alert = new Alert( AlertType.CONFIRMATION, "O prato que você pensou é " + caracteristica, ButtonType.YES, ButtonType.NO );
        configureDialog( alert );

        Optional< ButtonType > result = alert.showAndWait();
        // TODO Se ! result.isPresent "fechou o dialog" não deveria cancelar o programa?
        return result.isPresent() && result.get() == ButtonType.YES;
    }

    @Override
    public boolean confirmaNome( String nome ) {

        Alert alert = new Alert( AlertType.CONFIRMATION, "O prato escolhido foi " + nome, ButtonType.YES, ButtonType.NO );
        configureDialog( alert );

        Optional< ButtonType > result = alert.showAndWait();
        // TODO Se ! result.isPresent "fechou o dialog" não deveria cancelar o programa?
        return result.isPresent() && result.get() == ButtonType.YES;
    }

    @Override
    public void mostraMensagemEncontrado( String nome ) {

        Alert alert = new Alert( AlertType.INFORMATION, "Acertei denovo!" );
        configureDialog( alert );

        alert.showAndWait();
    }

    @Override
    public String perguntaNome() {

        TextInputDialog dialog = new TextInputDialog();
        configureDialog( dialog );
        dialog.setHeaderText( "Qual prato você pensou? " );
        Optional< String > optionalResult = dialog.showAndWait();
        // TODO Se não presente não deveria reclamar?
        if ( optionalResult.isPresent() ) {
            return optionalResult.get();
        }
        return "";
    }

    @Override
    public String perguntaCaracteristica( String nomeNovo, String nomeAnterior ) {

        TextInputDialog dialog = new TextInputDialog();
        configureDialog( dialog );
        dialog.setHeaderText( nomeNovo + " é _______ mas " + nomeAnterior + " não." );
        Optional< String > optionalResult = dialog.showAndWait();
        // TODO Se não presente não deveria reclamar?
        if ( optionalResult.isPresent() ) {
            return optionalResult.get();
        }
        return "";
    }

    private void configureDialog( Dialog< ? > dialog ) {

        dialog.setTitle( "Jogo Gourmet" );
        dialog.initOwner( this.getScene()
                              .getWindow() );
        dialog.getDialogPane()
              .setMinHeight( Region.USE_PREF_SIZE );
    }
}

package br.com.emorgado.jogo.gourmet.model;

import br.com.emorgado.jogo.gourmet.service.JogoGourmetCliente;

public class FinalizadorComAprendizado<E> implements FinalizadorJogada<E> {

    private JogoGourmetCliente cliente;
    private ElementoJogo< E > elementoJogo;
    
    public FinalizadorComAprendizado( JogoGourmetCliente cliente, ElementoJogo< E > elementoJogo) {
        
        this.cliente = cliente;
        this.elementoJogo = elementoJogo;
    }

    @Override
    public ElementoJogo<E> finalizaJogada() {
        
        String nome = cliente.perguntaNome();
        String caracteristica = cliente.perguntaCaracteristica( nome, elementoJogo.getNome().toString() );
        
        elementoJogo.setCaracteristica( new ElementoJogo<E>( elementoJogo.getNome() ) );
        elementoJogo.setProximo( new ElementoJogo< E >( (E)nome ) );
        elementoJogo.setNome( (E) caracteristica );
        return elementoJogo;
    }
}

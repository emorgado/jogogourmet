package br.com.emorgado.jogo.gourmet;

import br.com.emorgado.jogo.gourmet.service.JogoGourmet;
import br.com.emorgado.jogo.gourmet.view.JogoGourmetViewClient;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class App extends Application {

    private Image            appIcon;
    
    @Override
    public void init() throws Exception {
        appIcon = new Image( getClass().getResourceAsStream( "/images/appIcon.png" ) );
    }
    
    @Override
    public void start( Stage primaryStage ) throws Exception {
        
        
        JogoGourmet jogo = new JogoGourmet( "Lazanha", "massa", "Bolo de chocolate" );
        
        JogoGourmetViewClient clientView = new JogoGourmetViewClient( jogo );
        
        Scene scene = new Scene( clientView );
        
        primaryStage.setScene( scene );        
        primaryStage.setTitle( "Jogo Gourmet" );
        primaryStage.getIcons().add( appIcon );
        primaryStage.show();
        
    }
    
    
    public static void main( String[] args ) {
        launch( args );
    }

}
